<?php
namespace tweeterapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class User extends EloquentModel {

    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function tweets() {
        return $this->hasMany('tweeterapp\model\Tweet', 'author');
    }

    public function liked() {
        return $this->belongsToMany('tweeterapp\model\Tweet', 'like', 'user_id', 'tweet_id');
    }

    public function followedBy() {
        return $this->belongsToMany('tweeterapp\model\User', 'follow', 'followee', 'follower');
    }

    public function follows() {
        return $this->belongsToMany('tweeterapp\model\User', 'follow', 'follower', 'followee');
    }

    public static function getUserById($id_user) {
        $requete = self::findOrFail($id_user);

        return $requete;
    }

    public static function getUserByUsername($username) {
        $requete = self::where('username', '=', $username)->first();

        return $requete;
    }
}