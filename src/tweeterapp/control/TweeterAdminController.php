<?php

namespace tweeterapp\control;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use mf\auth\exception\AuthentificationException as AuthentificationException;
use \mf\control\AbstractController as AbstractController;
use tweeterapp\model as Model;
use tweeterapp\view\TweeterView as View;
use tweeterapp\auth\TweeterAuthentification as TweeterAuthentification;
use mf\router\Router as Router;

class TweeterAdminController extends AbstractController {

    public function login($message=null) {
        $view = new View(null);
        $view->setAppTitle("Login");
        $view->render('login', $message);
    }

    public function signup() {
        $view = new View(null);
        $view->setAppTitle("Signup");
        $view->render('signup');
    }

    /* checkLogin()
     * 
     * Algo :
     * - On vérifie que les champs de connexions sont remplis
     * - On applique un filtre sur le nom d'utilisateur pour le normaliser et éviter les injections
     * - On vérifie l'existence de l'utilisateur dans la base
    */
    public function checkLogin() {
        $router = new Router();

        if(isset($_POST["login_button"])) {
            if(isset($_POST["username"]) && isset($_POST["password"])) {
                $auth = new TweeterAuthentification();
                $username = filter_var($_POST["username"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                try {
                    $auth->loginUser($username, $_POST["password"]);
                    $router->executeRoute('followsTweets');
                } catch (AuthentificationException $e) {
                    $message = $e->getMessage();
                    $router->executeRoute('loginUser', $message);
                }
            }
        }
        else {
            $router->executeRoute('loginUser');
        }
    }

    /* checkSignup()
     * 
     * Algo :
     * - On vérifie que les champs d'inscription sont remplis
     * - On applique un filtre sur le nom d'utilisateur et le nom complet éviter les injections et
     * les caractères spéciaux
     * - On crée l'utilisateur
    */
    public function checkSignup() {
        $router = new Router();

        if(isset($_POST["login_button"])) {
            if(isset($_POST["username"]) && isset($_POST["fullname"]) && isset($_POST["password"]) && isset($_POST["password_verify"])) {
                if($_POST["password"] == $_POST["password_verify"]) {
                    $auth = new TweeterAuthentification();
                    $username = filter_var($_POST["username"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                    $fullname = filter_var($_POST["fullname"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                    $auth->createUser($username, $_POST["password"], $fullname);
                    $router->executeRoute('accueil');
                }
                else {
                    $router->executeRoute('signupUser');
                }
            }
            else {
                $router->executeRoute('signupUser');
            }
        }
        else {
            $router->executeRoute('signupUser');
        }
    }

    public function logout() {
        $router = new Router();
        $auth = new TweeterAuthentification();
        $auth->logout();
        $router->executeRoute('accueil');
    }
}
