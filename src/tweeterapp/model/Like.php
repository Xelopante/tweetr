<?php
namespace tweeterapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Like extends EloquentModel {

    protected $table = 'like';
    protected $primaryKey = 'id';
    public $timestamps = false;
}