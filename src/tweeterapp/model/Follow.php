<?php
namespace tweeterapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Follow extends EloquentModel {

    protected $table = 'follow';
    protected $primaryKey = 'id';
    public $timestamps = false;
}