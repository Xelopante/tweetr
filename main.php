<?php
//Initialisation de l'affichae des erreurs
ini_set('display_errors', '1');

//Initialisation de la session
session_start();

//Require des autoloader
require_once('vendor/autoload.php');
require_once('src/mf/utils/AbstractClassLoader.php');
require_once('src/mf/utils/ClassLoader.php');

$loader = new mf\utils\ClassLoader('src');
$loader->register();

//Alias du manager de l'ORM Eloquent et des modèles tweeterapp
use Illuminate\Database\Capsule\Manager as EloquentManager;
use tweeterapp\model as Model;
use tweeterapp\control\TweeterController as Controller;
use mf\router\Router as Router;
use tweeterapp\view\TweeterView as View;
use tweeterapp\auth\TweeterAuthentification as TweeterAuthentification;

//Configuration de la base pour Eloquent
$config = parse_ini_file("conf/config.ini");

$eloquent = new EloquentManager();
$eloquent->addConnection($config);
$eloquent->setAsGlobal();
$eloquent->bootEloquent();

//Ajout des feuilles de styles dans la vue
View::addStyleSheet('html/style.css');

//Ajout des routes dans le routeur (alias, url, contrôleur, méthode)
$router = new Router();
$router->addRoute('accueil', '/home/', '\tweeterapp\control\TweeterController', 'viewHome', TweeterAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('viewTweet', '/view/', '\tweeterapp\control\TweeterController', 'viewTweet', TweeterAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('viewUserTweets', '/user/', '\tweeterapp\control\TweeterController', 'viewUserTweets', TweeterAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('loginUser', '/login/', '\tweeterapp\control\TweeterAdminController', 'login', TweeterAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('signupUser', '/signup/', '\tweeterapp\control\TweeterAdminController', 'signup', TweeterAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('checkLoginUser', '/check_login/', '\tweeterapp\control\TweeterAdminController', 'checkLogin', TweeterAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('checkSignupUser', '/check_signup/', '\tweeterapp\control\TweeterAdminController', 'checkSignup', TweeterAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('logoutUser', '/logout/', '\tweeterapp\control\TweeterAdminController', 'logout', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('viewForm', '/post/', '\tweeterapp\control\TweeterController', 'viewSendForm', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('postTweet', '/send/', '\tweeterapp\control\TweeterController', 'postTweet', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('personnalPage', '/following/', '\tweeterapp\control\TweeterController', 'viewPersonnalPage', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('like', '/like/', '\tweeterapp\control\TweeterController', 'likeTweet', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('follow', '/follow/', '\tweeterapp\control\TweeterController', 'followUser', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('followers', '/followers/', '\tweeterapp\control\TweeterController', 'viewFollowers', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('followsTweets', '/follows_tweets/', '\tweeterapp\control\TweeterController', 'viewFollowsTweets', TweeterAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('adminPage', '/admin/', '\tweeterapp\control\TweeterController', 'viewAdminPage', TweeterAuthentification::ACCESS_LEVEL_ADMIN);
$router->addRoute('followerFollowers', '/follower_followers/', '\tweeterapp\control\TweeterController', 'viewFollowerFollowers', TweeterAuthentification::ACCESS_LEVEL_ADMIN);
$router->setDefaultRoute('/home/');
$router->run();

?>