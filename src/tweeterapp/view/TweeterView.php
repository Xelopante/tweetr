<?php

namespace tweeterapp\view;
use mf\router\Router as Router;
use tweeterapp\auth\TweeterAuthentification as TweeterAuthentification;
use mf\utils\HttpRequest as HttpRequest;

class TweeterView extends \mf\view\AbstractView {
  
    /* Constructeur 
    *
    * Appelle le constructeur de la classe parent
    */
    public function __construct( $data ){
        parent::__construct($data);
    }

    /* Méthode renderHeader
     *
     *  Retourne le fragment HTML de l'entête (unique pour toutes les vues)
     */ 
    private function renderHeader(){
        return '<h1>TweeTR</h1>';
    }
    
    /* Méthode renderFooter
     *
     * Retourne le fragment HTML du bas de la page (unique pour toutes les vues)
     */
    private function renderFooter(){
        return '<p>La super app créée en Licence Pro &copy;2021<br />By Hittin Yann &#x1F60E; and Chevalot Lucas &#128081;</p>';
    }

    /* Méthode renderHome
     *
     * Vue de la fonctionalité afficher tous les Tweets. 
     *  
     */
    
    private function renderHome(){

        /*
         * Retourne le fragment HTML qui affiche tous les Tweets. 
         *  
         * L'attribut $this->data contient un tableau d'objets tweet.
         * 
         */
        
        $router = new Router();

        $html = "<article class='theme-backcolor2'>
                    <h2>Last Tweets</h2>";

                    
        foreach($this->data as $tweet) {
            $user = $tweet->user()->first();
            $html .=    "<div class='tweet'>
                            <a href=".$router->urlFor('viewTweet', [['id', $tweet->id]]).">$tweet->text</a>
                            <div class='tweet-footer'>
                                <span class='tweet-timestamp'>
                                    ".explode(" ", $tweet->created_at)[1]." | ".explode(" ", $tweet->created_at)[0]."
                                </span>
                                <span class='tweet-author'>
                                    <a href=".$router->urlFor('viewUserTweets', [['id', $tweet->author]]).">".$user->username."</a>
                                </span>
                            </div>
                        </div>";
        }

        $html .= "</article>";

        return $html;
    }
  
    /* Méthode renderUeserTweets
     *
     * Vue de la fonctionalité afficher tout les Tweets d'un utilisateur donné. 
     * 
     */
     
    private function renderUserTweets(){

        /* 
         * Retourne le fragment HTML pour afficher
         * tous les Tweets d'un utilisateur donné. 
         *  
         * L'attribut $this->data contient un objet User.
         *
         */

        $router = new Router();

        $tweets = $this->data->tweets()->orderBy('id', 'desc')->get();

        $html = "<article class='theme-backcolor2'>
                    <h2>".$this->data->username." Tweets</h2>";

        foreach($tweets as $tweet) {
            $html .=    "<div class='tweet'>
                            <a href=".$router->urlFor('viewTweet', [['id', $tweet->id]]).">$tweet->text</a>
                            <div class='tweet-footer'>
                                <span class='tweet-timestamp'>
                                    ".explode(" ", $tweet->created_at)[1]." | ".explode(" ", $tweet->created_at)[0]."
                                </span>
                                <span class='tweet-author'>
                                    ".$this->data->username."
                                </span>
                            </div>
                        </div>";
        }

        $html .= "</article>";

        return $html;
    }

    private function renderFollowsTweets(){

        /*
         * Retourne le fragment HTML qui affiche tous les Tweets. 
         *  
         * L'attribut $this->data contient un tableau d'objets tweet.
         * 
         */
        
        $router = new Router();

        $html = "<article class='theme-backcolor2'>
                    <h2>Follows Tweets</h2>";

        foreach($this->data as $user) {
            foreach($user->tweets as $tweet) {
                $html .=    "<div class='tweet'>
                                <a href=".$router->urlFor('viewTweet', [['id', $tweet->id]]).">$tweet->text</a>
                                <div class='tweet-footer'>
                                    <span class='tweet-timestamp'>
                                        ".explode(" ", $tweet->created_at)[1]." | ".explode(" ", $tweet->created_at)[0]."
                                    </span>
                                    <span class='tweet-author'>
                                        <a href=".$router->urlFor('viewUserTweets', [['id', $tweet->author]]).">".$user->username."</a>
                                    </span>
                                </div>
                            </div>";
            }
        }

        $html .= "</article>";

        return $html;
    }

    private function renderPersonnalPage() {

        $router = new Router();

        $html = "<article class='theme-backcolor2'>
                    <h2>Currently following</h2>
                    <ul id='followees'>";

        foreach($this->data["follows"] as $user) {
            $html .=    "<li>
                            <a href=".$router->urlFor('viewUserTweets', [['id', $user->id]]).">
                                ".$user->username."
                            </a>
                        </li>";
        }

        $html .=   "</ul>
                    <hr>
                    <h2>Followers</h2>
                    <span>".$this->data["nb_followers"]."</span>";

        if($this->data["nb_followers"] > 0) {
            $html .= "
                    <br />
                    <span><a href=".$router->urlFor('followers').">Show followers</a></span>";
        }

                    
        $html .= "</article>";

        return $html;
    }

    private function renderAdminPage() {
        
        $router = new Router();

        $html = "<article class='theme-backcolor2'>
                    <h2>Followers (filtrered by number of followers)</h2>
                    <ul id='followees'>";

        foreach($this->data as $follower) {
            $html .=    "<li>
                            <a href=".$router->urlFor('followerFollowers', [['id', $follower->id]]).">
                                ".$follower->username."
                            </a>
                            <span>".$follower->followers."</span>
                        </li>";
        }

        $html .= "  </ul>
                </article>
        ";

        return $html;
    }

    private function renderFollowerFollowers() {

        $router = new Router();

        $html = "<article class='theme-backcolor2'>
                    <h2>".$this->data["requested_follower"]->username."'s followers</h2>
                    <ul id='followees'>";

        foreach($this->data["follower_followers"] as $follower) {
            $html .=    "<li>
                            <a href=".$router->urlFor('viewUserTweets', [['id', $follower->id]]).">
                                ".$follower->username."
                            </a>
                        </li>";
        }

        $html .= "  </ul>
                </article>
        ";

        return $html;
    }

    private function renderFollowers() {
        $router = new Router();

        $html = "<article class='theme-backcolor2'>
                    <h2>Followers</h2>
                    <ul id='followees'>";

        foreach($this->data as $follower) {
            $html .=    "<li>
                            <a href=".$router->urlFor('viewUserTweets', [['id', $follower->id]]).">
                                ".$follower->username."
                            </a>
                        </li>";
        }

        $html .= "
                    </ul>
                </article>
        ";

        return $html;
    }

    /* Méthode renderViewTweet 
     * 
     * Rréalise la vue de la fonctionnalité affichage d'un tweet
     *
     */
    
    private function renderViewTweet($message){

        /* 
         * Retourne le fragment HTML qui réalise l'affichage d'un tweet 
         * en particulié 
         * 
         * L'attribut $this->data contient un objet Tweet
         *
         */

        $router = new Router();
        $auth = new TweeterAuthentification();
        $http_req = new HttpRequest();

        $user = $this->data["tweet"]->user()->first();

        $html = "<article class='theme-backcolor2'>";

        $html .=    "<div class='tweet'>
                        ".$this->data["tweet"]->text."
                        <div class='tweet-footer'>
                            <span class='tweet-timestamp'>
                                    ".explode(" ", $this->data["tweet"]->created_at)[1]." | ".explode(" ", $this->data["tweet"]->created_at)[0]."
                            </span>
                            <span class='tweet-author'>
                                <a href=".$router->urlFor('viewUserTweets', [['id', $user->id]]).">".$user->username."</a>
                            </span>
                        </div>
                        <div class='tweet-footer'>
                            <hr>
                            <span class='tweet-score tweet-control'>".$this->data["tweet"]->score."</span>
                            ";
                            
                        if($auth->logged_in) {
                            $html .= "
                            <a class='tweet-control' href='".$router->urlFor('like', [['id', $http_req->get['id']]])."'>
                                <img alt='Like' src='".$http_req->root."/images/";

                            $html .= ($this->data['like_status']) ? "full_heart.png" : "empty_like.png" ;

                            $html .= "' /></a>
                            <a class='tweet-control' href='".$router->urlFor('follow', [['id', $http_req->get['id']]])."'>
                                <img alt='Follow' src='".$http_req->root."/images/";
                            
                            $html .= ($this->data['follow_status']) ? "followed.png" : "follow.png" ;
                            
                            $html .= "' /></a>";
                        }
                            
                        $html .= "
                        </div>
                    </div>";

        if ($message !== null) {
            $html .= "<span class='event-message'>".$message."</span>";
        }
                
        $html .= "</article>";

        return $html;
    }



    /* Méthode renderPostTweet
     *
     * Realise la vue de régider un Tweet
     *
     */
    protected function renderPostTweet(){
        
        /* Méthode renderPostTweet
         *
         * Retourne la framgment HTML qui dessine un formulaire pour la rédaction 
         * d'un tweet, l'action (bouton de validation) du formulaire est la route "/send/"
         *
         */

        $router = new Router();

        $html = "<article class='theme-backcolor2'>
        <h2>New Tweet</h2>";

        $html .= "<form method='POST' action=".$router->urlFor('postTweet').">
                    <textarea id='tweet-form' name='text' placeholder='Enter your tweet' maxlength='140'></textarea>
                    <div>
                        <input id='send_button' type='submit' name='send' value='Send' />
                    </div>
                </form>";

        $html .= "</article>";

        return $html;
        
    }

    private function renderLogin($message) {

        $router = new Router();

        $html = "<article class='theme-backcolor2'>
        <h2>Login</h2>";

        $html .= "<form class='forms' action=".$router->urlfor('checkLoginUser')." method='POST'>
                    <input class='forms-text' type='text' name='username' placeholder='Username' />
                    <input class='forms-text' type='password' name='password' placeholder='Password' />
                    <input class='forms-text' type='submit' name='login_button' value='Login'/>
                </form>";

        if ($message !== null) {
            $html .= "<span class='event-message'>".$message."</span>";
        }

        $html .= "</article>";

        return $html;
    }

    private function renderSignup() {

        $router = new Router();

        $html = "<article class='theme-backcolor2'>
        <h2>Sign up</h2>";

        $html .= "<form class='forms' action=".$router->urlfor('checkSignupUser')." method='POST'>
                    <input class='forms-text' type='text' name='fullname' placeholder='Fullname' />
                    <input class='forms-text' type='text' name='username' placeholder='Username' />
                    <input class='forms-text' type='password' name='password' placeholder='Password' />
                    <input class='forms-text' type='password' name='password_verify' placeholder='Retype password' />
                    <input class='forms-text' type='submit' name='login_button' value='Login'/>
                </form>";

        $html .= "</article>";

        return $html;
    }

    private function renderTopMenu() {
        
        $router = new Router();
        $auth = new TweeterAuthentification();
        $http_req = new HttpRequest();

        $html = "<header class='theme-backcolor1'>".$this->renderHeader()."
                    <nav id='navbar'>
                        <a class='tweet-control' href=".$router->urlFor('accueil')."><img alt='home' src='".$http_req->root."/images/home.png'></a>";
        
        if($auth->logged_in) {
            $html .= "
                    <a class='tweet-control' href=".$router->urlFor('followsTweets')."><img alt='follows_network' src='".$http_req->root."/images/follows_network.png'></a>
                    <a class='tweet-control' href=".$router->urlFor('personnalPage')."><img alt='following' src='".$http_req->root."/images/follows.png'></a>";

                    if($auth->access_level >= TweeterAuthentification::ACCESS_LEVEL_ADMIN) {
                        $html .= "<a class='tweet-control' href=".$router->urlFor('adminPage')."><img alt='admin' src='".$http_req->root."/images/admin.png'></a>";
                    }

            $html .= "<a class='tweet-control' href=".$router->urlFor('logoutUser')."><img alt='logout' src='".$http_req->root."/images/logout.png'></a>";
        }
        else {
            $html .= "
                    <a class='tweet-control' href=".$router->urlFor('loginUser')."><img alt='login' src='".$http_req->root."/images/login.png'></a>
                    <a class='tweet-control' href=".$router->urlFor('signupUser')."><img alt='signup' src='".$http_req->root."/images/signup.png'></a>";
        }

        $html .= "</nav>
        </header>";

        return $html;
    }

    private function renderBottomMenu() {

        $router = new Router();

        $html = "<nav id='menu' class='theme-backcolor1'>
                    <div id='nav-menu'>
                        <div class='button theme-backcolor2'>
                            <a href=".$router->urlFor('viewForm').">New</a>
                        </div>
                    </div>
                </nav>";

        return $html;
    }

    /* Méthode renderBody
     *
     * Retourne la framgment HTML de la balise <body> elle est appelée
     * par la méthode héritée render.
     *
     */
    
    protected function renderBody($selector, $message){

        /*
         * voire la classe AbstractView
         * 
         */
        $auth = new TweeterAuthentification();

        $html = $this->renderTopMenu();

        $html .= "<section>";
        switch($selector) {
            case "user":
                $html .= $this->renderUserTweets();
                break;
            case "home":
                $html .= $this->renderHome();
                break;
            case "view":
                $html .= $this->renderViewTweet($message);
                break;
            case "post":
                $html .= $this->renderPostTweet();
                break;
            case "login":
                $html .= $this->renderLogin($message);
                break;
            case "signup":
                $html .= $this->renderSignup();
                break;
            case "personnal_page":
                $html .= $this->renderPersonnalPage();
                break;
            case "follows_tweets":
                $html .= $this->renderFollowsTweets();
                break;
            case "followers":
                $html .= $this->renderFollowers();
                break;
            case "admin":
                $html .= $this->renderAdminPage();
                break;
            case "follower_followers":
                $html .= $this->renderFollowerFollowers();
                break;
            default:
                $html .= $this->renderHome();
                break;
        }

        if($auth->logged_in) {
            $html .= $this->renderBottomMenu();
        }

        $html .= "</section>";

        $html .= "<footer class='theme-backcolor1'>".$this->renderFooter()."</footer>";

        return $html;
    }











    
}
