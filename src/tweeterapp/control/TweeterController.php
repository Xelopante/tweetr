<?php

namespace tweeterapp\control;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;  
use \mf\control\AbstractController as AbstractController;
use tweeterapp\model as Model;
use tweeterapp\view\TweeterView as View;
use tweeterapp\auth\TweeterAuthentification as TweeterAuthentification;

/* Classe TweeterController :
 *  
 * Réalise les algorithmes des fonctionnalités suivantes: 
 *
 *  - afficher la liste des Tweets 
 *  - afficher un Tweet
 *  - afficher les tweet d'un utilisateur 
 *  - afficher la le formulaire pour poster un Tweet
 *  - afficher la liste des utilisateurs suivis 
 *  - évaluer un Tweet
 *  - suivre un utilisateur
 *   
 */

class TweeterController extends AbstractController {


    /* Constructeur :
     * 
     * Appelle le constructeur parent
     *
     * c.f. la classe \mf\control\AbstractController
     * 
     */
    
    public function __construct(){
        parent::__construct();
    }


    /* Méthode viewHome : 
     * 
     * Réalise la fonctionnalité : afficher la liste de Tweet
     * 
     */
    
    public function viewHome(){

        /* Algorithme :
        *  
        *  1 Récupérer tout les tweet en utilisant le modèle Tweet
        *  2 Parcourir le résultat 
        *      afficher le text du tweet, l'auteur et la date de création
        *  3 Retourner un block HTML qui met en forme la liste
        * 
        */
        
        $tweets = Model\Tweet::getAllTweets();

        $view = new View($tweets);
        $view->setAppTitle('Home');
        $view->render('home');
    }


    /* Méthode viewTweet : 
     *  
     * Réalise la fonctionnalité afficher un Tweet
     *
     */
    
    public function viewTweet($message = null){

        /* Algorithme : 
         *  
         *  1 L'identifiant du Tweet en question est passé en paramètre (id) 
         *      d'une requête GET 
         *  2 Récupérer le Tweet depuis le modèle Tweet
         *  3 Afficher toutes les informations du tweet 
         *      (text, auteur, date, score)
         *  4 Retourner un block HTML qui met en forme le Tweet
         * 
         *  Erreurs possibles : (*** à implanter ultérieurement ***)
         *    - pas de paramètre dans la requête
         *    - le paramètre passé ne correspond pas a un identifiant existant
         *    - le paramètre passé n'est pas un entier 
         * 
         */
        
        if(isset($this->request->get['id'])) {
            $auth = new TweeterAuthentification();

            $tweet = Model\Tweet::getTweetById($this->request->get['id']);
            $current_user = Model\User::getUserByUsername($auth->user_login);
            $tweet_author = $tweet->user()->first();

            /*On récupère le status de like du tweet de l'utilisateur et le status d'abonnement à l'auteur du tweet sous forme de booléen
            Afin d'adapter les icônes en conséquence dans la vue*/
            $follow_status = ($tweet_author->followedBy()->find($current_user['id']) !== null) ? true : false;
            $like_status = ($tweet->likedBy()->find($current_user['id']) !== null) ? true : false;

            $data = ["tweet" => $tweet, "follow_status" => $follow_status, "like_status" => $like_status];

            $view = new View($data);
            $view->setAppTitle($tweet_author->username."'s tweet");
            $view->render('view', $message);
        }
    }

    /*
     * likeTweet()
     * 
     * Algorithme :
     * - On vérifie que l'utilisateur est authentifié
     * - On récupère les données de l'utilisateur actuel, le tweet sur lequel il est, et l'auteur du tweet
     * - Si l'utilisateur n'a pas liké le tweet :
     *      - On insére une ligne dans la table pivot "like" (on like)
     *      - On incrémente le nombre de like du tweet
     * - Sinon
     *      - On enlève la ligne qui correspond au like dans la table pivot "like" (on n'aime plus)
     *      - On retire 1 au nombre de likes du tweet (score)
     * - On sauvegarde les données du tweet en desactivant les timestamps (ne fonctionne pas ¯\_(ツ)_/¯)
     * - On retourne sur la vue du tweet avec un message informant de la dernière action effectuée
    */
    public function likeTweet() {
        $auth = new TweeterAuthentification();
        $message = null;

        if($auth->user_login != null) {
            $current_user = Model\User::getUserByUsername($auth->user_login);
            $tweet = Model\Tweet::getTweetById($this->request->get['id']);
            $tweet_author = $tweet->user()->first();

            if($tweet->likedBy()->find($current_user['id']) == null) {
                $current_user->liked()->attach($tweet['id']);
                $tweet->score++;
                $message = "Tweet liked";
            }
            else {
                $current_user->liked()->detach($tweet['id']);
                $tweet->score--;
                $message = "Tweet unliked";
            }
            
            $tweet->save(["timestamps" => false]);

            $this->viewTweet($message);
        }
    }

    /*
     * followUser()
     * 
     * Algorithme :
     * - On vérifie que l'utilisateur est authentifié
     * - On récupère les données de l'utilisateur actuel, le tweet sur lequel il est, et l'auteur du tweet
     * - Si l'utilisateur ne suit pas l'auteur du tweet :
     *      - On insére une ligne dans la table pivot "follow" (on s'abonne)
     *      - On incrémente le nombre de followers de l'auteur du tweet
     * - Sinon
     *      - On enlève la ligne qui correspond à l'abonnement dans la table pivot "follow" (on se désabonne)
     *      - On retire 1 au nombre de followers de l'auteur du tweet
     * - On sauvegarde les données de l'auteur du tweet
     * - On retourne sur la vue du tweet avec un message informant de la dernière action effectuée
    */
    public function followUser() {
        $auth = new TweeterAuthentification();
        $message = null;

        if($auth->user_login != null) {
            $tweet = Model\Tweet::getTweetById($this->request->get['id']);

            $current_user = Model\User::getUserByUsername($auth->user_login);
            $tweet_author = $tweet->user()->first();

            if($tweet_author->id !== $current_user->id) {
                if($tweet_author->followedBy()->find($current_user['id']) == null) {
                    $current_user->follows()->attach($tweet_author['id']);
                    $tweet_author->followers++;
                    $message = "You are now following ".$tweet_author['username'];
                }
                else {
                    $current_user->follows()->detach($tweet_author['id']);
                    $tweet_author->followers--;
                    $message = "You are no longer following ".$tweet_author['username'];
                }
    
                $tweet_author->save();
            }
            else {
                $message = "You cannot follow yourself";
            }

            $this->viewTweet($message);
        }
    }


    /* Méthode viewUserTweets :
     *
     * Réalise la fonctionnalité afficher les tweet d'un utilisateur
     *
     */
    
    public function viewUserTweets(){

        /*
         *
         *  1 L'identifiant de l'utilisateur en question est passé en 
         *      paramètre (id) d'une requête GET 
         *  2 Récupérer l'utilisateur et ses Tweets depuis le modèle 
         *      Tweet et User
         *  3 Afficher les informations de l'utilisateur 
         *      (non, login, nombre de suiveurs) 
         *  4 Afficher ses Tweets (text, auteur, date)
         *  5 Retourner un block HTML qui met en forme la liste
         *
         *  Erreurs possibles : (*** à implanter ultérieurement ***)
         *    - pas de paramètre dans la requête
         *    - le paramètre passé ne correspond pas a un identifiant existant
         *    - le paramètre passé n'est pas un entier 
         * 
         */

        if(isset($this->request->get['id'])) {
            $user = Model\User::getUserById($this->request->get['id']);

            $view = new View($user);
            $view->setAppTitle($user->username."'s tweets");
            $view->render('user');
        }
        
    }

    public function viewSendForm() {
        $view = new View(null);
        $view->setAppTitle("Post a new tweet");
        $view->render('post');
    }

    /*
     * viewPersonnalPage()
     * 
     * Algorithme :
     * - On vérifie que l'utilisateur est authentifié
     * - On récupère les données de l'utilisateur actuel
     * - On récupère la liste de ses followees
     * - On revoie la liste des followees ainsi que le nombre de followers de l'utilisateur actuel
    */
    public function viewPersonnalPage() {
        $auth = new TweeterAuthentification();

        if($auth->user_login != null) {
            $current_user = Model\User::getUserByUsername($auth->user_login);
            $follows = $current_user->follows()->get();

            $data = ["follows" => $follows, "nb_followers" => $current_user->followers];

            $view = new View($data);
            $view->setAppTitle("Personnal page");
            $view->render('personnal_page');
        }
    }

    public function postTweet() {
        if(isset($_POST["send"])) {
            if(isset($_POST["text"]) && ($_POST["text"] !== null)) {
                $auth = new TweeterAuthentification();

                if($auth->user_login != null) {
                    $user = Model\User::getUserByUsername($auth->user_login);
                    $tweet = new Model\Tweet();
                    $tweet->text = filter_var($_POST["text"], FILTER_SANITIZE_SPECIAL_CHARS);
                    $tweet->author = $user->id;
                    $tweet->save();

                    $tweets = Model\Tweet::getAllTweets();

                    $view = new View($tweets);
                    $view->setAppTitle("Home");
                    $view->render('home');
                }
            }
        }
    }

    /*
     * viewFollows()
     * 
     * Algorithme :
     * - On vérifie que l'utilisateur est authentifié
     * - On récupère les données de l'utilisateur actuel
     * - On récupère la liste de ses followers ainsi que tous leurs tweets (en 1 seules requête avec with() pour éviter de surcharger la BDD de requêtes dans une boucle avec des get() sur chaque followee)
     * - On envoie les tweets des followees dans la vue
    */
    public function viewFollowsTweets() {
        $auth = new TweeterAuthentification();

        if($auth->user_login != null) {
            $user = Model\User::getUserByUsername($auth->user_login);

            $follows_tweets = $user->follows()->with('tweets')->orderBy('id', 'desc')->get();

            $view = new View($follows_tweets);
            $view->setAppTitle("Followees tweets");
            $view->render('follows_tweets');
        }
    }

    /*
     * viewFollowers()
     * 
     * Algorithme :
     * - On vérifie que l'utilisateur est authentifié
     * - On récupère les données de l'utilisateur actuel
     * - On récupère la liste de ses followers
     * - On envoie la liste des followers dans la vue
    */
    public function viewFollowers() {
        $auth = new TweeterAuthentification();

        if($auth->user_login != null) {
            $current_user = Model\User::getUserByUsername($auth->user_login);

            $followers = $current_user->followedBy()->get();

            $view = new View($followers);
            $view->setAppTitle("Followers");
            $view->render('followers');
        }
    }

    /*
     * viewAdminPage()
     * 
     * Algorithme :
     * - On vérifie que l'utilisateur est authentifié
     * - On récupère les données de l'utilisateur actuel
     * - On récupère la liste de ses followers triés par leur nombre de followers par ordre décroissant
     * - On envoie la liste des followers dans la vue
    */
    public function viewAdminPage() {
        $auth = new TweeterAuthentification();

        if($auth->user_login != null) {
            $current_user = Model\User::getUserByUsername($auth->user_login);

            $followers = $current_user->followedBy()->orderBy('followers', 'desc')->get();

            $view = new View($followers);
            $view->setAppTitle("Admin stats page");
            $view->render('admin');
        }
    }

    /*
     * viewFollowerFollowers()
     * 
     * Algorithme :
     * - On vérifie que l'utilisateur est authentifié
     * - On récupère le follower par son ID passé en paramètre
     * - On récupère la liste des followers du follower
     * - On renvoie les infos du follower demandé et des ses followers
    */
    public function viewFollowerFollowers() {
        $auth = new TweeterAuthentification();

        if($auth->user_login != null) {
            $requested_follower = Model\User::getUserById($this->request->get['id']);

            $follower_followers = $requested_follower->followedBy()->get();

            $data = ["requested_follower" => $requested_follower, "follower_followers" => $follower_followers];

            $view = new View($data);
            $view->setAppTitle($requested_follower->username."'s followers");
            $view->render('follower_followers');
        }
    }
}
