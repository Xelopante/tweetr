<?php
namespace tweeterapp\model;
use \Illuminate\Database\Eloquent\Model as EloquentModel;

class Tweet extends EloquentModel {

    protected $table = 'tweet';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('tweeterapp\model\User', 'author');
    }

    public function likedBy() {
        return $this->belongsToMany('tweeterapp\model\User', 'like', 'tweet_id', 'user_id');
    }

    public static function getAllTweets() {

        $requete = self::orderBy('id', 'desc')->get();  /* exécution de la requête et plusieurs lignes résultat */

        return $requete;
    }

    public static function getTweetById($id) {
        $requete = self::findOrFail($id);

        return $requete;
    }

    public static function getNbLike($id) {
        $requete = self::findOrFail($id);

        return $requete;
    }
}